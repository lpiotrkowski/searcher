import bcrypt
from flask_jwt_extended import get_raw_jwt
from flask_jwt_extended import revoke_token
from flask_jwt_extended import create_access_token

from app import jwt
from db.database import session
from db.models import User
from exc import ClientError, UnauthorizedAccessError


def register_account(input_data):
    email = input_data.get('email')

    user = session.query(User).filter_by(email=email).first()

    if user:
        raise ClientError(message='Podany email istnieje w bazie', status_code=409)

    new_user = User(email=email, password=input_data.get('password'))
    __add_user(new_user)
    auth_token = create_access_token(identity=email)

    return auth_token


def sign_in(credentials):
    input_password = credentials.get('password')
    user = session.query(User).filter_by(email=credentials.get('email')).first()

    if not user:
        raise UnauthorizedAccessError('Nieprawidłowe dane logowania')

    hash_password = bcrypt.hashpw(input_password.encode('utf-8'), user.password.encode('utf-8'))

    if hash_password.decode('utf-8') != user.password:
        raise UnauthorizedAccessError('Nieprawidłowe dane logowania')

    auth_token = create_access_token(identity=user.email)

    return auth_token


def __add_user(user):
    try:
        session.add(user)
        session.commit()
    except Exception:
        raise ClientError(message='Błąd serwera. Spróbuj ponownie', status_code=500)


def revoke_current_auth_token():
    current_token = get_raw_jwt()
    jti = current_token['jti']
    revoke_token(jti)


@jwt.revoked_token_loader
@jwt.expired_token_loader
@jwt.invalid_token_loader
def unauthorized_access():
    raise UnauthorizedAccessError()
