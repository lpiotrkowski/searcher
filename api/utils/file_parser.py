from utils.url_searcher import NEW_LINE


def get_urls_from_input_file(input_file):
    urls = []

    for line in input_file.readlines():
        url_line = line.decode('utf-8').replace(NEW_LINE, '')

        if url_line:
            urls.append(url_line)

    return urls
