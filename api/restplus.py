from flask_restplus import Api
from exc import BaseError

api = Api(version='1.0', title='Searcher API')


def error_response(errors, message, status):
    return {'errors': errors, 'message': message}, status


@api.errorhandler
def handle_error(error):
    if issubclass(type(error), BaseError):
        return error_response(error.errors, error.message, error.status_code)
    else:
        return error_response([], str(error), 500)
