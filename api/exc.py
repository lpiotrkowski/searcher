class BaseError(Exception):
    def __init__(self, errors, message):
        Exception.__init__(self, message)
        self.message = message
        self.errors = errors or []


class ClientError(BaseError):
    def __init__(self, message, errors=None, status_code=400) -> None:
        BaseError.__init__(self, errors, message)
        self.status_code = status_code


class ValidationError(BaseError):
    def __init__(self, message, errors=None, status_code=400) -> None:
        BaseError.__init__(self, errors, message)
        self.status_code = status_code


class UnauthorizedAccessError(BaseError):
    def __init__(self, message='Nieautoryzowany dostęp', errors=None, status_code=401) -> None:
        BaseError.__init__(self, errors, message)
        self.status_code = status_code


class InvalidFileError(BaseError):
    def __init__(self, message, errors=None, status_code=400) -> None:
        BaseError.__init__(self, errors, message)
        self.status_code = status_code


class CeleryConnectionError(BaseError):
    def __init__(self, message='Błąd połączenia z asynchronicznym workerem', errors=None,
                 status_code=503) -> None:
        BaseError.__init__(self, errors, message)
        self.status_code = status_code
