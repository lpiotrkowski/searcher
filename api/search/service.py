from sqlalchemy import desc

from constants import CeleryTaskStatus
from db.models import SearchTask, SearchRow


def get_search_tasks(session):
    st = SearchTask

    return session.query(st).order_by(desc(st.id)).all()


def add_search_task(session, task_id, status, search_keys):
    search_task = SearchTask(task_id=task_id, status=status, search_keys=search_keys)

    session.add(search_task)
    session.commit()


def set_search_task_failure(session, task_id, error_info):
    st = SearchTask
    search_task = session.query(st).filter(st.task_id == task_id).one()

    search_task.status = CeleryTaskStatus.FAILURE
    search_task.error_info = str(error_info)

    session.add(search_task)
    session.commit()


def set_search_task_success(session, task_id, result):
    st = SearchTask
    search_task = session.query(st).filter(st.task_id == task_id).one()

    search_task.status = CeleryTaskStatus.SUCCESS
    search_task.rows = __get_search_rows_from_task_result(result)

    session.add(search_task)
    session.commit()


def update_search_status(session, task_id, status):
    st = SearchTask
    search_task = session.query(st).filter(st.task_id == task_id).one()

    search_task.status = status

    session.add(search_task)
    session.commit()


def __get_search_rows_from_task_result(task_result):
    search_rows = []

    for url, occurrences in task_result.items():
        search_rows.append(SearchRow(url=url, occurrences=occurrences))

    return search_rows
