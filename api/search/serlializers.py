from flask_restplus import fields
from restplus import api

occurences_model = api.model('Search row  occurrences response model', {
    'name': fields.String(required=True),
    'flag': fields.Boolean(required=True)
})

search_row = api.model('Search row response model', {
    'url': fields.String(required=False),
    'occurrences': fields.Nested(occurences_model)
})


search_task_get_response_model = api.model('Search task response model', {
    'searchKeys': fields.String(required=True, attribute='search_keys'),
    'status': fields.String(required=True),
    'errorInfo': fields.String(required=False, attribute='error_info'),
    'taskId': fields.String(required=True, attribute='task_id'),
    'rows': fields.List(fields.Nested(search_row), required=True)
})

search_task_post_response_model = api.model('Search task post response model', {
    'taskId': fields.String(required=True, attribute='task_id')
})
