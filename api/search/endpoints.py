from flask_jwt_extended import jwt_required
from flask_restplus import Resource
from kombu.exceptions import OperationalError

from exc import InvalidFileError, CeleryConnectionError, ValidationError
from restplus import api
from db.database import session
from search.serlializers import search_task_get_response_model, search_task_post_response_model
from search.service import get_search_tasks
from search.tasks import search_keywords_in_urls
from search.validator import search_post_args, is_allowed_file, validate_urls
from utils.file_parser import get_urls_from_input_file

ns = api.namespace('search', description='Endpoints connected with search')


@ns.route('/')
class Search(Resource):
    @api.marshal_with(search_task_get_response_model)
    @jwt_required
    def get(self):
        return get_search_tasks(session), 200

    @api.expect(search_post_args)
    @api.marshal_with(search_task_post_response_model)
    @jwt_required
    def post(self):
        args = search_post_args.parse_args()
        urls = []

        if is_allowed_file(args['file'].filename) is False:
            raise InvalidFileError(message='Nieprawidłowy format pliku')
        try:
            urls = get_urls_from_input_file(args['file'])
        except UnicodeDecodeError:
            raise InvalidFileError(message='Niepoprawne kododanie pliku')

        validation_errors = validate_urls(urls)

        if validation_errors:
            raise ValidationError(errors=validation_errors, message='Błąd walidacji')

        try:
            task = search_keywords_in_urls.delay(urls, args['searchKeys'])
            return {'task_id': task.task_id}, 201
        except OperationalError:
            raise CeleryConnectionError()
