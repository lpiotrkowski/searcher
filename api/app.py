import datetime

import redis
from flask import Flask
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from simplekv.memory.redisstore import RedisStore

import settings
from db.database import db, reset_database
from restplus import api


def setup_app(app=None, is_celery=False):
    if not is_celery:
        __register_blueprint(app)
    else:
        app = Flask(__name__)
        init_config_values(app)

    db.init_app(app)

    return app


def __register_blueprint(app):
    from flask import Blueprint

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)

    from search.endpoints import ns as search_namespace
    from auth.endpoints import ns as auth_namespace

    api.add_namespace(search_namespace)
    api.add_namespace(auth_namespace)

    app.register_blueprint(blueprint)

    CORS(app, resources=r'/api/*')


def __init_jwt_settings(app):
    app.config['SECRET_KEY'] = settings.JWT_SECRET_KEY
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(hours=10)
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_STORE'] = RedisStore(redis.StrictRedis())
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = 'all'


def init_config_values(app):
    app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
    app.config['CLEAN_AND_CREATE_DB'] = settings.CLEAN_AND_CREATE_DB
    app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    __init_jwt_settings(app)
#

flask_app = Flask(__name__)
init_config_values(flask_app)
jwt = JWTManager(flask_app)


def run():
    setup_app(flask_app)
    flask_app.run()

if __name__ == "__main__":
    run()

    if flask_app.config['CLEAN_AND_CREATE_DB']:
        reset_database()
