from flask_sqlalchemy import SQLAlchemy
from celery import Celery
from app import setup_app
from settings import CELERY_REGISTERED_TASKS_SRC, CELERY_TASKS_FILENAME, CELERY_BROKER_URL


def create_celery(app=None):
    celery_inst = Celery(app.import_name, broker=CELERY_BROKER_URL)
    celery_inst.conf.update(app.config)
    TaskBase = celery_inst.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery_inst.Task = ContextTask
    celery_inst.autodiscover_tasks(lambda: CELERY_REGISTERED_TASKS_SRC, related_name=CELERY_TASKS_FILENAME)

    return celery_inst

db = SQLAlchemy()
celery_app = setup_app(is_celery=True)
celery = create_celery(celery_app)
session = db.session
