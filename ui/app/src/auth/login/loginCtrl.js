'use strict';

angular.module('uiApp').controller('LoginCtrl', ['AuthService', '$location', LoginCtrl]);

function LoginCtrl(AuthService, $location) {
    var vm = this;

    vm.errorData = null;
    vm.model = {};

    vm.login = function () {
        vm.loader = true;

        AuthService.login(vm.model).then(
            function (response) {
                $location.path('/')
            }, function (response) {
                vm.errorData = response.data;
            });
    };
}
