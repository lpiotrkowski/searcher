'use strict';

angular.module('uiApp').controller('RegistrationCtrl', ['AuthService', '$location', RegistrationCtrl]);

function RegistrationCtrl(AuthService, $location) {
    var vm = this;

    vm.passwordError = false;
    vm.model = {};
    vm.errors = [];
    vm.loader = false;
    vm.errorData = null;

    vm.passwordsEquals = function(){
        return vm.model.password === vm.model.repeatPassword;
    };

    vm.register = function () {
        vm.loader = true;
        resetData();

        if(!vm.passwordsEquals()){
            vm.passwordError = true;
            return;
        }

        AuthService.register(vm.model).then(function(response){
            $location.path('/');
            vm.loader = false;
        }, function(response){
            vm.loader = false;
            vm.errorData = response.data;
        });
    };

    function resetData(){
         vm.passwordError = false;
          vm.errorData = null;
    }
}
