'use strict';

angular.module('uiApp').config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'src/search/search-list/search-tasks-list.html',
            controller: 'SearchTasksListCtrl',
            controllerAs: 'vm',
            bindToController: true,
            requireLogin: true
        })
        .when('/nowe-wyszuiwanie', {
            templateUrl: 'src/search/new-search-task/new-search-task.html',
            controller: 'NewSearchTaskCtrl',
            controllerAs: 'vm',
            bindToController: true,
            requireLogin: true
        })
        .when('/rejestracja', {
            templateUrl: 'src/auth/registration.html',
            controller: 'RegistrationCtrl',
            controllerAs: 'vm',
            bindToController: true,
            requireLogin: false
        })
        .when('/logowanie', {
            templateUrl: 'src/auth/login/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'vm',
            bindToController: true,
            requireLogin: false
        })
        .otherwise({
            redirectTo: '/'
        });
});


angular.module('uiApp').run(function ($rootScope, AuthService, $route, $location) {
    $rootScope.$on('$locationChangeStart', function (event, next, prev) {
        var location = $location.path();

        if ($route.routes[location].requireLogin && !AuthService.isLoggedIn()) {
            $location.path('/logowanie');
        }

        if (!$route.routes[location].requireLogin && AuthService.isLoggedIn()){
            $location.path('/');
        }
    });
});