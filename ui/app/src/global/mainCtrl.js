'use strict';

angular.module('uiApp').controller('MainCtrl', ['AuthService', MainCtrl]);

function MainCtrl(AuthService) {
    var vm = this;

    vm.isLoggedIn = AuthService.isLoggedIn;
}
