'use strict';

angular
  .module('uiApp', [
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngAnimate',
    'ngSanitize',
    'ngTouch',
      'ui.bootstrap'
  ]);
